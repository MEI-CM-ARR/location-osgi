package pt.ipleiria.estg.meicm.locationosgi.communication;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.Logger;
import pt.ipleiria.estg.meicm.locationosgi.constants.Properties.TopicToPublish;

import java.util.Dictionary;
import java.util.Hashtable;

public class EventAdminBridge
{
 private Logger logger;
 private EventAdmin eventAdmin;

 public EventAdminBridge(EventAdmin eventAdmin, Logger logger)
 {
  this.logger = logger;
  this.eventAdmin = eventAdmin;
  this.publish(TopicToPublish.Location,0);
 }

 public void publish(String topic, int locationId)
 {
  this.logger.info("Publish to topic " + topic);
  Dictionary<String,Integer> properties = new Hashtable<>(0);
  properties.put("locationID",locationId);
  Event reportGeneratedEvent = new Event(topic, properties);
  eventAdmin.postEvent(reportGeneratedEvent);
 }
}