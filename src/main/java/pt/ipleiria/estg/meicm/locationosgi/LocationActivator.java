package pt.ipleiria.estg.meicm.locationosgi;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.Logger;
import org.osgi.service.log.LoggerFactory;
import pt.ipleiria.estg.meicm.locationosgi.communication.EventAdminBridge;
import pt.ipleiria.estg.meicm.locationosgi.helper.LogWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class LocationActivator implements BundleActivator
{
 private Logger logger;
 private EventAdmin eventAdmin;

 public void start(BundleContext bundleContext) throws IOException, NullPointerException
 {
  this.initiateLogging(bundleContext);
  this.initiateLogWriter(bundleContext);
  this.initiateEventAdmin(bundleContext);

  if(this.logger == null || this.eventAdmin == null)
  {
   throw new NullPointerException("Logger or EventAdminBridge are null");
  }
  new EventAdminBridge(this.eventAdmin,this.logger);
 }

 public void stop(BundleContext bundleContext)
 {
  if(this.logger != null)
  {
   this.logger.debug("The " + bundleContext.getBundle().getSymbolicName() + " stop");
  }
 }

 private void initiateLogging(BundleContext context)
 {
  ServiceReference<LoggerFactory> ref = context.getServiceReference(LoggerFactory.class);
  if(ref == null)
  {
   return;
  }
  LoggerFactory loggerFactory = context.getService(ref);
  this.logger = loggerFactory.getLogger("pt.ipleiria.estg.meicm.location-osgi");
 }

 private void initiateEventAdmin(BundleContext context)
 {
  ServiceReference<EventAdmin> ref = context.getServiceReference(org.osgi.service.event.EventAdmin.class);
  if(ref == null)
  {
   throw new NullPointerException("ServiceReference<EventAdminBridge> ref is null");
  }

  Bundle bundle = ref.getBundle();
  if(bundle == null)
  {
   throw new NullPointerException("Bundle bundle is null");
  }

  try
  {
   if(bundle.getState() == Bundle.INSTALLED || bundle.getState() == Bundle.RESOLVED)
   {
    bundle.start();
   }
  }
  catch(BundleException e)
  {
   this.logger.error("Error catch: " + e.getMessage());
  }

  this.eventAdmin = context.getService(ref);
 }

 private void initiateLogWriter(BundleContext context) throws FileNotFoundException
 {
  ServiceReference<LogReaderService> ref = context.getServiceReference(LogReaderService.class);
  if(ref == null)
  {
   this.logger.error("Cannot get LoggerFactory service");
   return;
  }
  LogReaderService reader = context.getService(ref);
  File logFile = context.getDataFile("log.log");
  reader.addLogListener(new LogWriter(logFile));
 }
}