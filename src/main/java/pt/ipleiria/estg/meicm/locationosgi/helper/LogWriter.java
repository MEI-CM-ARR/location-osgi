package pt.ipleiria.estg.meicm.locationosgi.helper;

import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class LogWriter implements LogListener
{
 private DataOutputStream fileOutputStream;
 public LogWriter(File file) throws FileNotFoundException
 {
  this.fileOutputStream = new DataOutputStream(new FileOutputStream(file));
 }

 public void logged(LogEntry logEntry)
 {
  try
  {
   this.fileOutputStream.writeBytes(logEntry.getBundle().getSymbolicName());
   this.fileOutputStream.writeBytes(": ");
   this.fileOutputStream.writeBytes(logEntry.getMessage());
   this.fileOutputStream.writeBytes("\n");
  }
  catch(IOException e)
  {
   e.printStackTrace();
  }
 }
}
